import React from 'react';
import CharacterList from './CharacterList';
import * as UTIL from '../util';

export default class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Pokedex',
    };
    constructor(props) {
        super(props);
        this.state = {characters:[]};
    }
    componentDidMount() {
        this.getList();
    }
    getList = () => {
        fetch(UTIL.getJsonUrl())
        .then((response) => response.json())
        .then((responseJson) => this.setList(responseJson))
        .catch((error) => {
            console.error(error);
        });
    }
    setList = (json) => {
        this.setState({characters: json});
    }
    render() {
        const {navigate} = this.props.navigation;
        return <CharacterList characters={this.state.characters} navigation={navigate} />
    }
}