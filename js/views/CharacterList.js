import React from 'react';
import {StyleSheet, FlatList, Text, View, Image, Dimensions, TouchableOpacity} from 'react-native';
import * as UTIL from '../util';

export default class CharacterList extends React.PureComponent {
    _keyExtractor = (item, index) => item.id;

    _onPressItem = (id) => {
        const selectedCharacters = this.props.characters.filter((character) => character.id === id);
        if (selectedCharacters.length > 0) {
            const selectedCharacter = selectedCharacters[0];
            this.props.navigation('Character', {character: selectedCharacter});
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor={this._keyExtractor}
                    data={this.props.characters}
                    extraData={this.state}
                    renderItem={({item}) => <Character model={item} onPressItem={this._onPressItem} />}
                    numColumns="3"
                    style={styles.flex}
                />
            </View>
        );
    }
}

class Character extends React.PureComponent {
    _onPress = () => {
        this.props.onPressItem(this.props.model.id);
    }
    render() {
        const model = this.props.model;
        if (!model) {
            return;
        }
        const imageUrl = UTIL.getThumbnailUrl(model);
        const types = model.type;
        const colour = UTIL.colorFromType(types[0]);
        const colour2 = types.length > 1 ? UTIL.colorFromType(types[1]) : null;
        var viewStyle = styles.item;
        viewStyle['backgroundColor'] = colour;
        return (
            <TouchableOpacity onPress={this._onPress}>
                <View style={[styles.item, {backgroundColor: colour}]}>
                    <View style={[styles.itemTypeBg, colour2 !== null && {backgroundColor: colour2, opacity: 1}]}/>
                    <Image source={{uri: imageUrl}} style={styles.characterImage} />
                    <Text style={styles.characterText}>{UTIL.getName(model)}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const screenWidth = Dimensions.get('window').width;
const itemDimen = 100;
const colCount = 3;
const margin = (screenWidth - (itemDimen * colCount)) / (colCount + 1);

const styles = StyleSheet.create({
    container: {
     flex: 1
    },
    flex: {
        paddingTop: 20
    },
    item: {
        backgroundColor: '#fff',
        alignItems:'stretch',
        color: '#fff',
        borderRadius: itemDimen / 2,
        marginLeft: margin,
        marginBottom: 20,
        fontSize: 18,
        height: itemDimen,
        width: itemDimen,
        textAlign: 'center',
        textAlignVertical: 'center',
        overflow: 'hidden'
    },
    itemTypeBg: {
        opacity: 0,
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        width: itemDimen / 2,
        zIndex: -1
    },
    characterImage: {
        height: itemDimen - 40,
        margin: 20,
        marginTop: 10,
        width: itemDimen - 40
    },
    characterText: {
        color: '#fff',
        backgroundColor: '#33333344',
        textAlign: 'center',
        fontSize: 12,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        paddingTop: 5,
        paddingBottom: 15
    }
});