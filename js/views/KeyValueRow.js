import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default class KeyValueRow extends React.Component {
    render() {
        return (
            <View style={[styles.root, this.props.isEven && styles.rootBg]}>
                <Text style={styles.keyText}>{this.props.keyText}</Text>
                <Text styles={styles.valueText}>{this.props.valueText}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        color: '#333',
        flex: 1,
        flexDirection: 'row',
        padding: 6,
    },
    rootBg: {
        backgroundColor: '#f7f7f7',
    },
    keyText: {
        flex: 1,
        fontSize: 18,
        fontWeight: 'bold',
    },
    valueText: {
        flex: 1,
        fontSize: 16,
        paddingTop: 2,
        textAlign: 'right',
    }
});