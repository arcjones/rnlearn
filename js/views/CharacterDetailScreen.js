import React from 'react';
import {ScrollView, StyleSheet, Text, View, Image} from 'react-native';
import SectionHeaderView from './SectionHeaderView';
import KeyValueRow from './KeyValueRow';
import * as UTIL from '../util';

export default class CharacterDetailScreen extends React.Component {
	static navigationOptions = ({ navigation }) => {
		const character = navigation.getParam('character', null);
		return {
			title: character !== null ? character.name.english : 'Character',
		};
	};
	render() {
		const {navigation} = this.props;
		const character = navigation.getParam('character', null);
		if (character === null) {
			return null;
		}
		var color1 = character.type.length > 0 ? UTIL.colorFromType(character.type[0]) : null;
		var color2 = character.type.length > 1 ? UTIL.colorFromType(character.type[1]) : null;
		var typeString = "";
		character.type.forEach((type, index) => {
			if (index > 0) {
				typeString = typeString + ', ';
			}
			typeString = typeString + type;
		});
		return (
			<ScrollView>
				<View style={[styles.character, color1 && {backgroundColor: color1}]}>
					<View style={[styles.characterTypeBg, color2 && {backgroundColor: color2}]} />
					<Image source={{uri: UTIL.getImageUrl(character)}} style={styles.characterImage} />
				</View>
				<SectionHeaderView text={'Details'}/>
				<KeyValueRow keyText={'#'} valueText={UTIL.padId(character.id)}/>
				<KeyValueRow keyText={'Name (English)'} valueText={character.name.english} isEven/>
				<KeyValueRow keyText={'Name (Japansese)'} valueText={character.name.japanese}/>
				<KeyValueRow keyText={'Name (Chinese)'} valueText={character.name.chinese} isEven/>
				<KeyValueRow keyText={'Type'} valueText={typeString}/>
				<SectionHeaderView text={'Stats'}/>
				<KeyValueRow keyText={'Attack'} valueText={character.base.Attack}/>
				<KeyValueRow keyText={'Defense'} valueText={character.base.Defense} isEven/>
				<KeyValueRow keyText={'HP'} valueText={character.base.HP}/>
				<KeyValueRow keyText={'Speed'} valueText={character.base.Speed} isEven/>
				<KeyValueRow keyText={'Sp. Attack'} valueText={character.base['Sp. Attack']}/>
				<KeyValueRow keyText={'Sp. Defense'} valueText={character.base['Sp. Defense']} isEven/>
			</ScrollView>
		);
	}
}

const imageDimen = 180;
const imagePadding = 20;
const styles = StyleSheet.create({
	character: {
		height: imageDimen + (imagePadding * 2),
		justifyContent: 'center',
		alignItems: 'center',
	},
	characterImage: {
		height: imageDimen,
		margin: imagePadding,
		width: imageDimen
	},
	characterTypeBg: {
		position: 'absolute',
		top: 0,
		right: 0,
		bottom: 0,
		width: '50%',
		zIndex: -1
	},
	characterText: {
		color: '#fff',
		backgroundColor: '#33333344',
		textAlign: 'center',
		fontSize: 12,
		position: 'absolute',
		left: 0,
		right: 0,
		bottom: 0,
		paddingTop: 5,
		paddingBottom: 15
	}
});