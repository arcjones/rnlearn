import React from 'react';
import {StyleSheet, Text} from 'react-native';

export default class SectionHeaderView extends React.Component {
    render() {
        return (
            <Text style={styles.sectionHeader}>{this.props.text}</Text>
        );
    }
}

const styles = StyleSheet.create({
    sectionHeader: {
        color: '#fff',
        backgroundColor: '#333333',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 8,
    }
});