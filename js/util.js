export function padId(id) {
  const padded = '00' + id;
  return padded.substr(padded.length - 3);
}

export function colorFromType(type) {
  switch(type.toLowerCase()) {
    case "grass":
      return "#78C850";
    case "poison":
        return "#A040A0"
    case "fire":
        return "#F08030"
    case "flying":
        return "#A890F0"
    case "water":
        return "#6890F0"
    case "bug":
        return "#A8B820"
    case "normal":
        return "#A8A878"
    case "electric":
        return "#F8D030"
    case "ground":
        return "#E0C068"
    case "fairy":
        return "#EE99AC"
    case "fighting":
        return "#C03028"
    case "psychic":
        return "#F85888"
    case "rock":
        return "#B8A038"
    case "steel":
        return "#B8B8D0"
    case "ice":
        return "#98D8D8"
    case "ghost":
        return "#705898"
    case "dragon":
        return "#7038F8"
    case "dark":
        return "#705848"
    default:
      return "#fff";
  }
}

export function getName(model) {
    return model.name.english;// To be expanded if a language preference is stored.
}

const baseUrl = 'https://raw.githubusercontent.com/fanzeyi/pokemon.json/master/';

export function getJsonUrl() {
    return baseUrl + 'pokedex.json';
}

export function getThumbnailUrl(model) {
    return baseUrl + 'thumbnails/' + getImageName(model);
}

export function getImageUrl(model) {
    return baseUrl + 'images/' + getImageName(model);
}

function getImageName(model) {
    const name = JSON.stringify(getName(model)).replace(' ', '_').replace( /\W/g , '');
    return padId(model.id) + name + '.png';
}

