import React, {Component} from 'react';
import {Platform, FlatList, StyleSheet, Text, View, Dimensions} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import HomeScreen from './views/HomeScreen';
import CharacterDetailScreen from './views/CharacterDetailScreen';

const MainNavigator = createStackNavigator({
    Home: {screen: HomeScreen},
    Character: {screen: CharacterDetailScreen}
  },{
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#336633',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      }
    },
    headerLayoutPreset: 'center',
  }
);

const App = createAppContainer(MainNavigator);
export default App;